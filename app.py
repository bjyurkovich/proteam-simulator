from flask import Flask, jsonify, request
import os

app = Flask(__name__)

PORT = os.getenv("PORT")


projects = [
    {
        "userId": 2,
        "id": 1,
        "name": "Datacenter Build"
    },
    {
        "userId": 1,
        "id": 2,
        "name": "Place to eat build"
    }
]

requisitions = []

# http://localhost:5000/requester/1
@app.route("/requester/<userId>", methods=["GET"])
def get_requester(userId):

    # algorithm
    userId = int(userId) + 5

    return jsonify({"projectNumber": "1234", "productDescription": "asdf", "userId": userId})


@app.route("/users/<userId>/projects", methods=["GET"])
def get_projects_for_user(userId):

    # algorithm
    userId = int(userId)
    user_projects = []
    for project in projects:
        if project["userId"] == userId:
            user_projects.append(project)

    return jsonify(user_projects)


@app.route("/requisitions", methods=["POST"])
def create_new_requisition():
    body = request.json

    print(body)

    requisitions.append(body)
    return jsonify({"status": "ok"})


@app.route("/requisitions", methods=["GET"])
def get_requisitions():
    return jsonify(requisitions)


if __name__ == "__main__":
    app.run(debug=True, port=PORT, host="0.0.0.0")
